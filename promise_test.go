package promise

import (
	"testing"
	"time"

	"github.com/dop251/goja"
)

type testHelper struct {
	vm      *goja.Runtime
	factory *Factory
	tasks   chan func()
	done    chan struct{}
}

func (t *testHelper) runResult(script string) error {
	t.done = make(chan struct{})

	_, err := t.vm.RunScript("testScript.js", script)
	if err != nil {
		return err
	}

	for {
		select {
		case task := <-t.tasks:
			task()
		case <-t.done:
			return nil
		}
	}
}

func (t *testHelper) jsTimeout(call goja.FunctionCall) goja.Value {
	var timeout uint64
	err := t.vm.ExportTo(call.Argument(0), &timeout)
	if err != nil {
		panic(err)
	}

	value := call.Argument(1)

	p, obj := t.factory.Create()
	time.AfterFunc(time.Duration(timeout)*time.Millisecond, func() {
		p.Fulfill(value)
	})
	return obj
}

func newHelper(t *testing.T) *testHelper {
	helper := &testHelper{
		vm:    goja.New(),
		tasks: make(chan func(), 100),
	}
	helper.factory = Enable(helper.vm, helper.tasks)

	helper.vm.Set("timeout", helper.jsTimeout)
	helper.vm.Set("testDone", func() { close(helper.done) })
	helper.vm.Set("error", t.Error)
	helper.vm.Set("log", t.Log)

	return helper
}

func TestThrowOnFulfilled(t *testing.T) {
	const script = `
		Promise.resolve().then(function() {
			throw "test value"
		}).catch(function(value) {
			if (value != "test value") {
				error("value was " + value)
			}
		}).catch(error)
		.finally(testDone)
	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestResolveFulfilled(t *testing.T) {
	const script = `
		var p = Promise.resolve("test value")

		timeout(100).then(function() {
			return p
		}).then(function(value) {
			if (value != "test value") {
				error("value was " + value)
			}
		}).catch(error)
		.finally(testDone)
	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestFulfillTwice(t *testing.T) {
	const script = `
		var p = new Promise(function(resolve, reject) {
			resolve("first")
			resolve("second")
		}).then(function(value) {
			if (value != "first") {
				error("value was " + value)
			}
		}).catch(error)
		.finally(testDone)

	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestRejectTwice(t *testing.T) {
	const script = `
		var p = new Promise(function(resolve, reject) {
			reject("first")
			reject("second")
		}).catch(function(value) {
			if (value != "first") {
				error("value was " + value)
			}
		}).catch(error)
		.finally(testDone)

	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestResolveRejected(t *testing.T) {
	const script = `
		var p = Promise.reject("test value")

		timeout(100).then(function() {
			return p
		}).then(undefined, function(value) {
			if (value != "test value") {
				error("value was " + value)
			}
		}).catch(error)
		.finally(testDone)
	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestThrowOnRejected(t *testing.T) {
	const script = `
		Promise.reject().then(undefined, function() {
			throw "test value"
		}).catch(function(value) {
			if (value != "test value") {
				error("value was " + value)
			}
		}).catch(error)
		.finally(testDone)
	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestPromiseThen(t *testing.T) {
	const script = `
		Promise.resolve("test value")
			.then(function(value) {
				return timeout(1000, value + " and another value")
			})
			.then(function(value) {
				if (value != "test value and another value") {
					error("value is " + value)
				}
			}).catch(function (err) {
				error(err)
			}).finally(testDone)
	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestThenEmpty(t *testing.T) {
	const script = `
		Promise.resolve("test value")
			.then()
			.then(function(value) {
				return timeout(1000, value + " and another value")
			})
			.then(function(value) {
				if (value != "test value and another value") {
					error("value is " + value)
				}
			}).catch(function (err) {
				error(err)
			}).finally(testDone)
	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestThenEmptyAPI(t *testing.T) {
	h := newHelper(t)
	p, pObj := h.factory.Create()
	p.Then(nil, nil)
	p.Then(func(goja.Value) {
		close(h.done)
	}, nil)

	p.Fulfill(goja.Undefined())

	h.vm.Set("p", pObj)

	err := h.runResult("p.catch(error).finally(testDone)")

	if err != nil {
		t.Error(err)
	}
}

func TestThenEmptyAPIReject(t *testing.T) {
	h := newHelper(t)
	p, pObj := h.factory.Create()
	p.Then(nil, nil)
	p.Then(func(goja.Value) {
		close(h.done)
	}, nil)

	p.Reject(h.vm.ToValue("test value"))

	h.vm.Set("p", pObj)

	err := h.runResult(`p.catch(function(value) {
		if (value != "test value") {
			error("value was " + value)
		}
	}).catch(error)
	.finally(testDone)`)

	if err != nil {
		t.Error(err)
	}
}

func TestPromiseFulfilledThen(t *testing.T) {
	const script = `
		var p = Promise.resolve("test value")
		timeout(100).then(function() {
			return p.then(function(value) {
				if (value != "test value") {
					error("value was " + value)
				}
			})
		}).catch(error)
		.finally(testDone)
	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestPromiseRejectedThen(t *testing.T) {
	const script = `
		var p = Promise.reject("test value")
		timeout(100).then(function() {
			return p.catch(function(value) {
				if (value != "test value") {
					error("value was " + value)
				}
			})
		}).catch(error)
		.finally(testDone)
	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestPromiseThenable(t *testing.T) {
	const script = `
		function createThenable(rejectValue, resolveValue) {
			return {
				then: function(resolve, reject) {
					if (rejectValue) {
						reject(rejectValue)
					}
					resolve(resolveValue)
				}
			}
		}

		Promise.resolve(createThenable(undefined, "resolve value"))
			.then(function(value) {
				if (value != "resolve value") {
					error("value was " + value)
				}
			})
			.catch(error)
			.finally(testDone)

	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestPromiseThenableThrow(t *testing.T) {
	const script = `
		function createThenable(rejectValue, resolveValue) {
			return {
				then: function(resolve, reject) {
					throw rejectValue
				}
			}
		}

		Promise.resolve(createThenable("resolve value"))
			.then(undefined, function(value) {
				if (value != "resolve value") {
					error("value was " + value)
				}
			})
			.catch(error)
			.finally(testDone)

	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestPromiseThenableReject(t *testing.T) {
	const script = `
		function createThenable(rejectValue, resolveValue) {
			return {
				then: function(resolve, reject) {
					if (rejectValue) {
						reject(rejectValue)
					}
					throw rejectValue // this error will be ignored
				}
			}
		}

		Promise.resolve(createThenable("resolve value"))
			.then(undefined, function(value) {
				if (value != "resolve value") {
					error("value was " + value)
				}
			})
			.catch(error)
			.finally(testDone)

	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestExecutor(t *testing.T) {
	const script = `
		var p = new Promise(function(resolve, reject) {
			log("In executor")
			timeout(1000)
				.then(resolve, reject)
		})

		p.catch(error)
		.finally(testDone)
	`
	const desiredResult = "test value"

	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestExecutorReject(t *testing.T) {
	const script = `
		var p = new Promise(function(resolve, reject) {
			timeout(100)
				.then(function() {
					reject("test value")
				})
		})

		p.catch(function(value) {
			if (value != "test value") {
				error("value was " + value)
			}
		})
		.finally(testDone)
	`

	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestExecutorThrow(t *testing.T) {
	const script = `
		var p = new Promise(function(resolve, reject) {
			throw "test value"
		})

		p.catch(function(value) {
			if (value != "test value") {
				error("value was " + value)
			}
		})
		.finally(testDone)
	`

	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestAll(t *testing.T) {
	const script = `
		var second = timeout(100).then(function() {return "second"})
		var third = timeout(200).then(function() {return "third"})
		Promise.all(["first", second, third]).then(function(result) {
			if (result[0] != "first") {
				error("result[0] was " + result[0])
			}

			if (result[1] != "second") {
				error("result[1] was " + result[1])
			}

			if (result[2] != "third") {
				error("result[2] was " + result[2])
			}
		})
		.catch(error)
		.finally(testDone)
	`

	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestAllEmpty(t *testing.T) {
	const script = `
		Promise.all().catch(error).finally(testDone)
	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestRace(t *testing.T) {
	const script = `
		var first = timeout(50).then(function() {return "first"})
		var second = timeout(100).then(function() {return "second"})
		var third = timeout(200).then(function() {return "third"})

		Promise.race([first, second, third])
			.then(function(value) {
				if (value != "first") {
					error("value was " + value)
				}
			}).catch(error)
			.finally(testDone)
	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestRaceEmpty(t *testing.T) {
	const script = `
		Promise.race().catch(error).finally(testDone)
	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestIsPromise(t *testing.T) {
	h := newHelper(t)

	_, pObj := h.factory.Create()

	if !IsPromise(pObj) {
		t.Errorf("IsPromise did not recognize %v", pObj)
	}

	npObj := h.vm.NewObject()
	npObj.Set("__wrapped__", goja.Undefined)

	if IsPromise(npObj) {
		t.Errorf("IsPromise did recognize %v as promise", npObj)
	}
}

func TestResolveToSelf(t *testing.T) {
	const script = `
		var p = new Promise(function(resolve) {
			resolve(p)
		})

		p.catch(function(err) {
			if (err.message != "can not resolve to itself") {
				error("message was " + err.message)
			}
		}).finally(testDone)
	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestEmptyFinally(t *testing.T) {
	const script = `
		var p = new Promise(function(resolve) {
			resolve(p)
		})

		p.catch(function(err) {
			if (err.message != "can not resolve to itself") {
				error("message was " + err.message)
			}
		}).finally()
		.finally(testDone)
	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}

func TestThrowFinally(t *testing.T) {
	const script = `
		var p = new Promise(function(resolve) {
			resolve(p)
		})

		p.catch(function(err) {
			if (err.message != "can not resolve to itself") {
				error("message was " + err.message)
			}
		}).finally(function() {
			throw "test value"
		}).catch(function(err) {
			if (err != "test value") {
				error("err was " + err)
			}
		}).finally(testDone)
	`
	h := newHelper(t)
	err := h.runResult(script)

	if err != nil {
		t.Error(err)
	}
}
